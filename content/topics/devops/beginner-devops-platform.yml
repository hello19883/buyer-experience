---
  title: New to DevOps? Here's why you need a DevOps platform
  description: DevOps platform
  partenttopic: devops
  twitter_image: /nuxt-images/open-graph/gitlab-blog-cover.png
  old_topics_header:
      data:
        title:  New to DevOps? Here's why you need a DevOps platform
        block:
          - metadata:
              id_tag: what-is-devops
            text: |
              A DevOps platform brings the entire process (and team) together in one place. Here's what DevOps beginners need to know.
  side_menu:
    anchors:
      text: "ON THIS PAGE"
      data:
        - text: Gain visibility and context
          href: "#gain-visibility-and-context"
          data_ga_name: gain visibility and context
          data_ga_location: side-navigation
          variant: primary
        - text: Take action more easily
          href: "#take-action-more-easily"
          data_ga_name: take action more easily
          data_ga_location: side-navigation
        - text: Track projects with epics and issues
          href: "#track-projects-with-epics-and-issues"
          data_ga_name: track-projects-with-epics-and-issues
          data_ga_location: side-navigation
        - text: Using labels to track and search project
          href: "#using-labels-to-track-and-search-projects"
          data_ga_name: using-labels-to-track-and-search-projects
          data_ga_location: side-navigation
        - text: Dashboards help you track metrics
          href: "#dashboards-help-you-track-metrics"
          data_ga_name: dashboards help you track metrics
          data_ga_location: side-navigation
        - text: Value stream analytics
          href: "#value-stream-analytics"
          data_ga_name: value stream analytics
          data_ga_location: side-navigation
        - text: Benefits of DevOps
          href: "#benefits-of-dev-ops"
          data_ga_name: benefits of dev ops
          data_ga_location: side-navigation
        - text: Start your DevOps journey
          href: "#start-your-dev-ops-journey"
          data_ga_name: start your dev ops journey
          data_ga_location: side-navigation
    hyperlinks:
      text: "MORE ON THIS TOPIC"
      data:
        - text: Test your DevOps platform knowledge
          href: /quiz/devops-platform/
          data_ga_location: header
          variant: tertiary
          icon: true
          data_ga_name: Test your DevOps platform knowledge
        - text: Create the ideal DevOps team structure
          href: /topics/devops/build-a-devops-team/
          data_ga_location: header
          variant: tertiary
          icon: true
          data_ga_name: Create the ideal DevOps team structure
        - text: Continuous integration in DevOps
          href: /topics/ci-cd/benefits-continuous-integration/
          data_ga_location: header
          variant: tertiary
          icon: true
          data_ga_name: Continuous integration in DevOps
        - text: Understand continuous integration and delivery
          href: /topics/ci-cd/
          data_ga_location: header
          variant: tertiary
          icon: true
          data_ga_name: Understand continuous integration and delivery       
    content:
      - name: copy
        data:
          block:
            - column_size: 8
              text: |
                Once you’re working in DevOps and gaining all the software development and deployment benefits that come along with it, the next step is to understand [the benefits of adopting a single, end-to-end DevOps platform](https://www.youtube.com/watch?v=wChaqniv3HI){data-ga-name="benefits of adopting" data-ga-location="body"}.


                As helpful as DevOps is, using a [DevOps platform](/blog/2021/09/02/the-journey-to-a-devops-platform/){data-ga-name="DevOps platform" data-ga-location="body"} deployed as a single application takes those gains to the next level, enabling teams to deliver more value to their organization with fewer headaches. A platform, which combines the ability to plan, develop, test, secure, and operate software all in a single application, empowers teams to deliver software faster, more efficiently, and more securely. And that [makes the business more competitive and more agile](/blog/2022/02/03/the-devops-platform-series-building-a-business-case/){data-ga-name="competitive and more agile" data-ga-location="body"}.


                A complete DevOps platform gives organizations everything they need to turn ideas into functioning, valuable, secure software without the time-consuming and costly headaches that multiple tools and multiple UXes bring. A single, end-to-end platform also gives teams one data store sitting underneath everything they do, and, regardless of the interface they are using, allows them to easily surface insights about developer productivity, workflow efficiency, and DevOps practice adoption—out of the box.


                If a DevOps team has an idea for new or better software, having a platform will make it easier and faster to get it out and into users’ hands.


                While there are many benefits of using an end-to-end DevOps platform, we’re focusing here on two major gains: visibility and actionability.

      - name: copy
        data:
          block:
            - header: Gain visibility and context
              column_size: 8
              text: |
                A DevOps platform gives users visibility in that it allows them to see and understand what’s happening in the organization, as well as context for those events. With insights that go much deeper than what a simple report or dashboard can offer, users can better understand where they, and others, are in a project, as well as their impact.
      - name: copy
        data:
          block:
            - header: Take action more easily
              column_size: 8
              text: |
                Actionability means users can take that contextual information and efficiently and quickly do something with it at the point of understanding. Users can move a project ahead more quickly because they don’t have to wait to have a synchronous conversation or meeting to review the new information.


                Here are a few ways that an end-to-end platform provides visibility and actionability.
      - name: copy
        data:
          block:
            - header: Track projects with epics and issues
              column_size: 8
              text: |
                In a DevOps platform, users are better able to communicate, plan work, and collaborate by using epics and issues. [Epics](https://docs.gitlab.com/ee/user/group/epics/){data-ga-name="Epics" data-ga-location="body"} are an overview of a project, idea, or workflow. Issues are used to organize and list out what needs to be done to complete the larger goal, to track tasks and work status, or work on code implementations.


                For instance, if managers want an overview of how multiple projects, programs, or products are progressing, they can get that kind of visibility by checking an epic, which will give them a high-level rollup view of what is being worked on, what has been completed, and what is on schedule or delayed.


                Users can call up an epic to quickly see what’s been accomplished and what is still under way, and then they can dig deeper into sub-epics and related issues for more information. [Issues](https://docs.gitlab.com/ee/user/project/issues/){data-ga-name="Issues" data-ga-location="body"} offer details about implementation of specific goals, trace collaboration on that topic, and show which parts of the initiative team members are taking on. Users also can see whether due dates have been met or have passed. Issues can be used to reassign pieces of work, give updates, make comments or suggestions, and see how the nuts and bolts are being created and moved around.
      - name: copy
        data:
          block:
            - header: Using labels to track and search projects
              column_size: 8
              text: |
                Labels are classification tags, which are often assigned colors and descriptive titles like bug, feature request, or docs to make them easy to understand. They are used in epics, issues, and merge requests to help users organize their work and ideas. They give users at-a-glance insight about what teams are working on a project, the focus of the work, and where it stands in the development lifecycle. Labels can be added and removed as work progresses to enable better tracking and searching.
      - name: copy
        data:
          block:
            - header: Dashboards help you track metrics
              column_size: 8
              text: |
                Dashboards are reporting tools that pull together metrics from multiple tools to create an at-a-glance view of projects, [security issues](/blog/2020/07/07/secure-stage-for-appsec/){data-ga-name="security issues" data-ga-location="body"}, the health of different environments, or requests coming in for specific departments or teams, for instance. DevOps platform users can set up live dashboards to see trends in real time, map processes, and track response times, [errors](/blog/2020/01/23/iteration-on-error-tracking/), and deployment speed. Dashboards also can be used to see alert statuses and how alerts are affecting specific applications or the business overall.
      - name: copy
        data:
          block:
            - header: Value stream analytics
              column_size: 8
              text: |
                For visibility without any customization required, there are [value stream analytics](/blog/2022/01/24/gitlab-value-stream-analytics/){data-ga-name="value stream analytics" data-ga-location="body"}. This interface automatically pulls in data to show users how long it takes the team to complete each stage in their workflow – across planning, development, deployment, and monitoring. This gives developers or product owners – or anyone who wants information on workflow efficiency –  [a look at high-level metrics](/stages-devops-lifecycle/value-stream-analytics/){data-ga-name="look at high-level metrics" data-ga-location="body"}, like deployment frequency. This is actionable information so it also shows what part of the project is taking the most time or what is holding up progress. Based on this information, the user can suggest changes, like moving milestones or assigning the work to someone new, and enact those changes with just one click.


                With a DevOps platform, teams have end-to-end visibility that also is actionable. By enabling users to find the information they need with the context they need and giving them the ability to make immediate changes, data becomes actionable. Using a single platform, teams can move projects along more quickly, iterate faster, and create more value and company agility.
      - name: old-benefits-icons
        data:
          title: Benefits of DevOps
          subtitle: |
            [Adopting a DevOps mode](/customers/axway-devops/){data-ga-name="Axway devops"}{data-ga-location="body"} breaks down barriers so that development and operations teams are no longer siloed and have a more efficient way to work across the entire development and application lifecycle. Without DevOps, organizations experience handoff friction, which delays the delivery of software releases and negatively impacts business results.

            The DevOps model is an organization’s answer to increasing operational efficiency, accelerating delivery, and innovating products. Organizations that have implemented a DevOps culture experience the benefits of increased collaboration, fluid responsiveness, and shorter cycle times.
          horizontal_rule: true
          column_size: 8
          benefits:
            - title: Collaboration
              icon:
                name: collaboration-alt-2
                variant: marketing
                alt: Collaboration Icon
              subtitle: |
                Adopting a DevOps model creates alignment between development and operations teams; handoff friction is reduced and everyone is all in on the same goals and objectives.
            - title: Fluid responsiveness
              icon:
                name: efficiency
                variant: marketing
                alt: Efficiency Icon
              subtitle: |
                More collaboration leads to real-time feedback and greater efficiency; changes and improvements can be implemented quicker and guesswork is removed.
            - title: Shorter cycle time
              icon:
                name: time
                variant: marketing
                alt: Time Icon
              subtitle: |
                  Improved efficiency and frequent communication between teams shortens cycle time; new code can be released more rapidly while maintaining quality and security.
      - name: old-topics-cta
        data:
          title: Start your DevOps journey
          subtitle: Starting and Scaling DevOps in the Enterprise
          text: |
            Sharing his pioneering insight on how organizations can transform their software development and delivery processes, Gary Gruver provides a tactical framework to implement DevOps principles in “Starting and Scaling DevOps in the Enterprise.”
          column_size: 8
          cta_one:
            text: Download your free copy
            link: /resources/scaling-enterprise-devops/
            data_ga_name: Download free copy - Starting and Scaling DevOps in the Enterprise
            data_ga_location: body
  components:
    - name: copy-resources
      data:
        title: Resources
        block:
          - text: |
              Here's a list of resources on DevOps that we find to be particularly helpful in understanding DevOps and implementation. We would love to get your recommendations on books, blogs, videos, podcasts and other resources that tell a great DevOps story or offer valuable insight on the definition or implementation of the practice.

              Please share your favorites with us by tweeting us [@gitlab](https://twitter.com/gitlab)!
            resources:
              video:
                header: Videos
                links:
                  - text: How DevOps leads transformation (GitLab Virtual Commit 2020 track)
                    link: https://www.youtube.com/playlist?list=PLFGfElNsQthbAbiHjRVNz1WwxbhLfeXXs
                  - text: Cloud-Native DevOps (GitLab Virtual Commit 2020 track)
                    link: https://www.youtube.com/playlist?list=PLFGfElNsQthb4FD4y1UyEzi2ktSeIzLxj
                  - text: DevOps tips and tricks (GitLab Virtual Commit 2020 track)
                    link: https://www.youtube.com/playlist?list=PLFGfElNsQthZ_LGh4EpGJduNd2nFhN5fn
                  - text: How to simplify DevOps
                    link: https://www.youtube.com/watch?v=TUwvgz-wsF4
              case_study:
                header: Case Studies
                links:
                  - text: Axway aims for elite DevOps status
                    link: /customers/axway-devops/
                  - text: Worldline and the importance of collaboration
                    link: /customers/worldline/
                  - text: The European Space Agency and DevOps
                    link: /customers/european-space-agency/
              report:
                header: Reports
                links:
                  - text: GitLab’s 2020 Global DevSecOps Survey
                    link: /developer-survey/
                  - text: Gartner on application release orchestration
                    link: /blog/2020/01/16/2019-gartner-aro-mq/
              podcast:
                header: Podcasts
                links:
                  - text: Arrested DevOps
                    link: https://www.arresteddevops.com/
              book:
                header: Books
                links:
                  - text: Leading the Transformation
                    link: https://www.amazon.com/Leading-Transformation-Applying-DevOps-Principles/dp/1942788010
                  - text: The Goal
                    link: https://www.amazon.com/The-Goal-Process-Ongoing-Improvement/dp/0884271951/
                  - text: Starting and Scaling DevOps in the Enterprise
                    link: https://www.amazon.com/Start-Scaling-Devops-Enterprise-Gruver/dp/1483583589/
                  - text: The Phoenix Project
                    link: https://www.amazon.com/The-Phoenix-Project-Helping-Business/dp/0988262509/
    - name: featured-media
      data:
        header: Suggested Content
        column_size: 4
        media:
          - title: "Auto DevOps 101: How we're making CI/CD easier"
            aos_animation: fade-up
            aos_duration: 500
            text: |
              VP of product strategy Mark Pundsack shares everything you need to know about Auto DevOps
            link:
              text: Learn more
              href: /blog/2019/10/07/auto-devops-explained/
            image:
              url: /nuxt-images/blogimages/autodevops.jpg
              alt:

          - title: A beginner's guide to continuous integration
            aos_animation: fade-up
            aos_duration: 1000
            text: |
              Here's how to help everyone on your team, like designers and testers, get started with GitLab CI.
            link:
              text: Learn more
              href: /blog/2018/01/22/a-beginners-guide-to-continuous-integration/
            image:
              url: /nuxt-images/blogimages/beginners-guide-to-ci.jpg
              alt:

          - title: Leading SCM, CI and Code Review in one application
            aos_animation: fade-up
            aos_duration: 1500
            text: |
              The most important tools for developers are SCM, CI and Code Review, and it is better to have them all together.
            link:
              text: Learn more
              href: /blog/2020/09/30/leading-scm-ci-and-code-review-in-one-application
            image:
              url: /nuxt-images/blogimages/scm-ci-cr.png
              alt:
