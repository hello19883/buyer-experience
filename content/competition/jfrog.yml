---
  title: GitLab vs JFrog
  hero:
    title: GitLab vs JFrog
    subtitle: How does GitLab compare to JFrog in the Package stage?
    icon:
      name: package
      alt: package Icon
      variant: marketing
      size: md
    crumbs:
      - title: DevOps maturity comparison
        href: /competition/
        data_ga_name: Competition
        data_ga_location: breadcrumb
      - title: GitLab vs. JFrog
  overview:
    comparisons:
      - title: GitLab
        image: /nuxt-images/developer-tools/harvey-balls/50.svg
      - title: JFrog
        image: /nuxt-images/developer-tools/harvey-balls/100.svg
    left_card:
      title: JFrog is a Universal Package Manager solution
      description: |
        GitLab competes with JFrog’s Pipelines in the Package stage. JFrog is near-complete maturity in most categories in the Package stage, offering a robust solution for Package Management and Software Delivery. However, it lacks the collaboration tools and visibility that is needed when planning features and Source Code Management in a DevOps process. Additionally, JFrog’s offering is not as flexible as GitLab CI. For example, with JFrog it is not possible to create dynamic behaviors. 
    right_card:
      title: Gitlab's product roadmap
      bullets:
        - title: Virtual registries, which will make it easier to migrate customers from JFrog Artifactory to GitLab
        - title: Dependency Firewall is a new category of focus, which will help to prevent unknown or unverified providers from introducing potential security vulnerabilities
        - title: Improved discoverability and visibility into how dependencies are being used and by whom/which projects
      button:
        text: GitLab releases
        link: /releases/
  analysis:
    side_by_side:
      stage: Package
      icon:
        name: package
        alt: package Icon
        variant: marketing
        size: md
      tabs:
        - title: Package Registry
          panes:
            - title: GitLab
              harveyball: /nuxt-images/developer-tools/harvey-balls/50.svg
              description: Every team needs a place to store their packages and dependencies. GitLab aims to provide a comprehensive solution, integrated into our single application, that supports package management for all commonly used languages and binary formats.
              sections:
                - title: Details
                  content: | 
                    * GitLab offers Group and Project-level  private registries for a variety of package formats (11). List of supported package managers [here](https://docs.gitlab.com/ee/user/packages/package_registry/#supported-package-managers)
                    * Using GitLab Package Registry enables different custom workflows. Such as one project as Package Registry or multiple different packages from one monorepo
                    * Using GitLab Package Registry brings with it the possibility to use GitLab CI/CD to build and publish packages and with that the flexibility to customize and adapt different workflows 


                - title: Improving our product capabilities
                  content: |
                    * Richer [metadata](https://gitlab.com/groups/gitlab-org/-/epics/6207) about the packages published in the Package Registry UI. With information about its security and compliance scans
                    * Provide Package registries aggregations, to group diverse registries under a single URL 
                    * Provide support for more packages 
                    * Make packages more discoverable via search in the UI
                    * In response to JFrog Edge Nodes there is an open [issue](https://gitlab.com/gitlab-org/gitlab/-/issues/215390) addressing how to improve product capabilities 


              button:
                title: Documentation
                link: https://docs.gitlab.com/ee/user/packages/package_registry/
            - title: JFrog
              harveyball: /nuxt-images/developer-tools/harvey-balls/100.svg
              description: JFrog Artifactory focuses on a binary-centric approach -  it provides package registry and  management for an extensive list of package types. Providing  metadata and security scans information accessible through its UI once these binaries are uploaded to its repository 
              sections:
                - title: Details
                  content: |
                    * JFrog is intentional about positioning themselves one step further after source code management. Claiming DevOps success is due to binary management and source code is a foundational step that plays a smaller role. Their focus becomes evident in the product UI and capabilities. Supporting as per documentation over 30 packages types and offering rich metadata about these ones. Metadata such as: security & compliance (Using JFrog Xray), type of license, number of downloads, version control, and a layered view of what is inside each binary
                    * JFrog Artifactory allows for distributed replication of packages or images included in a release using Edge nodes
                    * Offers [Artifactory Query language](https://www.jfrog.com/confluence/display/JFROG/Artifactory+Query+Language) to search artifacts and builds using different metadata fields, such as "license type"
                    * Lacks collaboration tools within the platform, any conversation around a package vulnerability would need to be taken to another tool

              button:
                title: Documentation
                link: https://www.jfrog.com/confluence/display/JFROG/Package+Management
        - title: Container Registry
          panes:
            - title: GitLab
              harveyball: /nuxt-images/developer-tools/harvey-balls/50.svg
              description: A secure and private registry for Docker images built-in to GitLab. Creating, pushing, and retrieving images works out of the box with GitLab CI/CD.
              sections:
                - title: Details
                  content: |
                    * GitLab Container Registry is not a standalone registry. Being tightly integrated with GitLab - every project can have  its own space to store its Docker Images 
                    * Container Registry variables can be used directly in GitLab CI. Enabling usage and creation  of Docker images for specific tags, environments or branches
                    * Container Registry integration with GitLab projects enables automatic creation of Docker images that can be used in GitLab CI by simply pushing Dockerfiles to the repository
                    * In conjunction with Dependency Proxy allows to proxy and cache DockerHub 
                    * Using GitLab Container Registry along with CI/CD strips off the need to authenticate with external docker hub making for less verbose configuration files reusing authentication mechanism 

                - title: Improving our product capabilities
                  content: |
                    * Provide richer metadata of what is in the Docker image in GitLab Container Registry UI 
                    * Enable creation of rules to protect images as with protected environments to avoid deletion


              button:
                title: Documentation
                link: https://docs.gitlab.com/ee/user/packages/container_registry/
            - title: JFrog
              harveyball: /nuxt-images/developer-tools/harvey-balls/100.svg
              description: JFrog Container Registry resides under  Repositories. Same technology utilized for Packages in Artifactory. Allows configuration of multiple Docker and Helm Registries with fine-grained access control 
              sections:
                - title: Details
                  content: |
                    * JFrog Container Registry documentation links to Artifactory documentation, stating that Container Registry is not a different product or technology but rather a subset of features built on top of Artifactory
                    * Image Containers as in the case of Packages/Package Managers, can be queried using [Artifactory Query language](https://www.jfrog.com/confluence/display/JFROG/Artifactory+Query+Language) and its docker layers visualized in the UI 
                    * Allows the creation of Virtual Docker repositories. This type of repositories aggregate images from different repositories (local & remote) and presents it under a single URL
                    * JFrog Container Registry documentation link to Artifactory documentation (the main pillar for packages) might lead to confusion in the learning path. As evidenced by a [question](https://stackoverflow.com/questions/58946718/what-is-the-difference-between-jfrog-container-registry-and-jfrog-artifactory#:~:text=Just%20a%20note%20on%20terminology,repositories%20across%20regions%20and%20teams.) on stackoverflow asking for clarification in terminology 
              button:
                title: Documentation
                link: https://www.jfrog.com/confluence/display/JFROG/JFrog+Container+Registry
        - title: Helm Chart Registry
          panes:
            - title: GitLab
              harveyball: /nuxt-images/developer-tools/harvey-balls/50.svg
              description: Kubernetes cluster integrations can take advantage of Helm charts to standardize their distribution and install processes. Supporting a built-in helm chart registry allows for better, self-managed container orchestration.
              sections:
                - title: Details
                  content: |
                    * Helm Packages can use GitLab Package Registry and follow same workflows previously [described](https://docs.gitlab.com/ee/user/packages/helm_repository/#use-cicd-to-publish-a-helm-package) for publishing and download
                - title: Improving our product capabilities
                  content: |
                    * Helm chart in the Package Registry is under development. This [epic](https://gitlab.com/groups/gitlab-org/-/epics/6366) describes the work in progress to make it production ready
                    * Direction [Page](https://about.gitlab.com/direction/package/)

              button:
                title: Documentation
                link: https://docs.gitlab.com/ee/user/packages/container_registry/#use-the-container-registry-to-store-helm-charts
            - title: JFrog
              harveyball: /nuxt-images/developer-tools/harvey-balls/75.svg
              description: Helm Chart Registries are powered by Artifactory. Allow use of local, private and virtual Helm Repositories 
              sections:
                - title: Details
                  content: |
                    * Similarly to Container Registry, being part of Artifactory brings it the possibility to View Helm Chart information in the UI
                    * Helm Charts can be deployed using cURL, JFrog CLI and wget
                    * There are references in the documentation to a [JFrog Helm Client](https://www.jfrog.com/confluence/display/JFROG/Kubernetes+Helm+Chart+Repositories#KubernetesHelmChartRepositories-UsingtheJFrogHelmClient) which is a fork from the official [Helm repository](https://github.com/JFrog/helm?_gl=1*u3pq9i*_ga*MTE4MDgzNTI2NC4xNjYwMzAyMzk5*_ga_SQ1NR9VTFJ*MTY2MTc2MTUzMi45LjEuMTY2MTc2MjkyNS40Ny4wLjA.) 
                    * A key point JFrog emphasizes is the enterprise features available for Helm Charts, such as: HA, repository replication for multi-site development and storage scalability.
              button:
                title: Documentation
                link: https://www.jfrog.com/confluence/display/JFROG/Kubernetes+Helm+Chart+Repositories#KubernetesHelmChartRepositories-UsingtheJFrogHelmClient
        - title: Dependency Proxy
          panes:
            - title: GitLab
              harveyball: /nuxt-images/developer-tools/harvey-balls/50.svg
              description: The GitLab Dependency Proxy can serve as an intermediary between your local developers and automation and the world of packages that need to be fetched from remote repositories. By adding a security and validation layer to a caching proxy, you can ensure reliability, accuracy, and auditability for the packages you depend on.
              sections:
                - title: Details
                  content: |
                    * GitLab Dependency Proxy currently [supports](https://docs.gitlab.com/ee/user/packages/dependency_proxy/) Docker Images. Reducing Docker hub calls and speeding up pipelines that pull Container images
                    * Direction [Page](https://about.gitlab.com/direction/package/)
                - title: Improving our product capabilities
                  content: |
                    * Dependency Proxy [Epic](https://gitlab.com/groups/gitlab-org/-/epics/2920) to make it complete 
              button:
                title: Documentation
                link: https://docs.gitlab.com/ee/user/packages/dependency_proxy/
            - title: JFrog
              harveyball: /nuxt-images/developer-tools/harvey-balls/75.svg
              description: JFrog’s Proxy Repository Caching makes use of Remote Repositories as Caching Proxy for packages and docker images  
              sections:
                - title: Details
                  content: |
                    * Remote Repositories can be combined with Local repositories under a Virtual Repository providing a cache layer for both 
                    * Requested files are stored in cache layer after being requested, not the entire Remote Repository 
                    * Certain package types support remote browsing directly from the UI. Docker Hub does not
              button:
                title: Documentation
                link: https://www.jfrog.com/confluence/display/JFROG/Remote+Repositories#RemoteRepositories-BrowsingRemoteRepositories
        - title: Dependency Firewall
          panes:
            - title: GitLab
              harveyball: /nuxt-images/developer-tools/harvey-balls/0.svg
              description:  GitLab ensures that the dependencies stored in your package registries conform to your corporate compliance guidelines. This means you can prevent your organization from using dependencies that are insecure or out of policy.
              sections:
                - title: Details
                  content: |
                    * Dependency Firewall currently not being offered
                    * However, GitLab already provides dependency scanning across a variety of languages to alert users of any known security vulnerabilities

                - title: Improving our product capabilities
                  content: |
                    * Epic: Make Dependency [Firewall MInimal](https://gitlab.com/groups/gitlab-org/-/epics/5133)
              button:
                title: Documentation
                link: https://about.gitlab.com/direction/package/
            - title: Jfrog
              harveyball: /nuxt-images/developer-tools/harvey-balls/75.svg
              description: Currently JFrog can't delay updates from Packages if they meet certain criteria that can suggest suspicious activity. Nevertheless using JFrog Xray, already downloaded packages in the repositories can be scanned before they are consumed. If the artifact has vulnerabilities its download is blocked 
              sections:
                - title: Details
                  content: |
                    * Block unscanned: JFrog Xray policy and rules can block unscanned artifacts to be downloaded if these ones haven't been scanned
                    * Block Download: Artifactory will block downloads of artifacts that meet preconfigured filter criteria
                    * Nexus offers Firewall for Artifactory as a [plugin](https://help.sonatype.com/fw/getting-started/jfrog-artifactory-setup) 
              button:
                title: Documentation
                link: https://www.jfrog.com/confluence/display/JFROG/Creating+Xray+Policies+and+Rules
        - title: Git LFS
          panes:
            - title: GitLab
              harveyball: /nuxt-images/developer-tools/harvey-balls/50.svg
              description: Git LFS (Large File Storage) is a Git extension, which reduces the impact of large files in your repository by downloading the relevant versions of them lazily. Specifically, large files are downloaded during the checkout process rather than during cloning or fetching.
              sections:
                - title: Details
                  content: |
                    * GitLab allows managing large files and stores them as a blob either in local disks or remote object storages
                    * Compatible and support for Git LFS Client 
                - title: Improving our product capabilities
                  content: |
                    * Enable and configure using GitLab UI 
              button:
                title: Documentation
                link: https://docs.gitlab.com/ee/topics/git/lfs/index.html
            - title: Jira
              harveyball: /nuxt-images/developer-tools/harvey-balls/100.svg
              description: Artifactory supports LFS. Provides an LFS server that works with Git LFS client
              sections:
                - title: Details
                  content: |
                    * Allows security and access control to LFS Repositories 
                    * It is possible to configure XRay scans on LFS
                    * Supports Git LFS Client to point to JFrog Artifactory 
              button:
                title: Documentation
                link: https://www.jfrog.com/confluence/display/JFROG/Git+LFS+Repositories