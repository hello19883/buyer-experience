---
  title: GitLab for Education
  description: We want students to use our most advanced features, so they can take that experience to their future workplaces. Learn more here!
  components:
    - name: 'solutions-hero'
      data:
        title: GitLab for Education
        subtitle: Bringing DevOps to your Campus
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          url: /solutions/education/join/
          text: Join the GitLab for Education Program
          data_ga_name: join education program
          data_ga_location: header
        image:
          image_url: /nuxt-images/solutions/education.jpeg
          alt:  "Image: gitlab for education"
          rounded: true
    - name: tabs-menu
      data:
        menus:
          - id_tag: get-involved
            text: Get Involved!
            data_ga_name: get involved
            data_ga_location: header
          - id_tag: education-case-studies
            text: Education Case Studies
            data_ga_name: education case studies
            data_ga_location: header
          - id_tag: devops-in-education
            text: DevOps In Education
            data_ga_name: devops in education
            data_ga_location: header
          - id_tag: gitlab-in-research
            text: GitLab In Academic Research
            data_ga_name: gitlab in research
            data_ga_location: header
    - name: copy-media
      data:
        block:
          - header: GitLab for Education
            text: |
              At GitLab, we believe **that every student can contribute!** Our mission is to promote GitLab and [DevOps](/topics/devops/){data-ga-name="devops" data-ga-location="body"} at educational institutions around the world. We aim to build a community of educators, learners, and researchers who are passionate about all things related to DevOps and GitLab.

              Beyond providing free, unlimited licenses of our top-tier functionality (SaaS or self-managed), we want to be a resource for you to learn more about DevOps and connect with others who share your interest. Please [click here](/solutions/education/edu-survey/){data-ga-name="edu survey" data-ga-location="body"} to download the results of our 2020 GitLab for Education survey!
            video:
              video_url: https://www.youtube.com/embed/C5dhVoRlbp8?enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
          - header: Get involved!
            miscellaneous: |
              We are thrilled to welcome you to the GitLab for Education Program! The GitLab for Education Program has over **1.25 million users of GitLab's top-tier at over 1,000 educational institutions in over 65+ countries**. We are the definitive source for bringing DevOps to your campus.
            metadata:
              id_tag: get-involved
    - name: link-cards
      data:
        subtitle: "Here are few things to get you started on your DevOps journey:"
        has_horizontal_rule: true
        cards:
          - title: Join the GitLab Forum
            description: Come on by and introduce yourself! We created a special Education category as a resource for our program members. Consider the forum your resource for Q&A on GitLab!
            icon:
              name: community-customers
              variant: marketing
              alt: Community Customers Icon
            url: https://forum.gitlab.com/c/gitlab-for-education/37
            data_ga_name: gitlab forum
            data_ga_location: body
          - title: Sign up for a Guest Lecture!
            description: Have a GitLab Team Member talk to your students about DevOps!
            icon:
              name: community-hall-of-fame
              variant: marketing
              alt: Community Hall Of Fame Icon
            url: https://forms.gle/y2r5o83i8z6rfJPh8
            data_ga_name: gitlab for education
            data_ga_location: body
          - title: Join us on Twitch!
            description: Follow our Education Evangelist on Twitch for demos, GitLab tips and Tricks, and Student Spotlight Interviews!
            icon:
              name: gitlab
              variant: marketing
              alt: GitLab Icon
            url: https://www.twitch.tv/metzinaround
          - title: Attend or Host a Meetup
            description: In a pinch for a lecture topic? Looking for an engaging speaker on DevOps? Check out GitLab Meetups! We make it super easy.
            icon:
              name: community-events
              variant: marketing
              alt: Community Events Icon
            url: /community/meetups/
            data_ga_name: meetups
            data_ga_location: body
          - title: Hackathon
            description: We host quarterly hackathons where you can make contributions, win prizes, and network with other contributors. Students encouraged!
            icon:
              name: community-hackaton
              variant: marketing
              alt: Community Hackaton Icon
            url: /community/hackathon/
            data_ga_name: hackaton
            data_ga_location: body
          - title: Become a contributor
            description: Are you a student looking to gain experience? Want to develop your skills wthin a support network? Check our our code contributor program to get started!
            icon:
              name: gitlab
              variant: marketing
              alt: GitLab Icon
            url: /community/contribute/
            data_ga_name: contribute
            data_ga_location: body
    - name: case-studies
      data:
        title: Education Case Studies
        has_horizontal_rule: true
        metadata:
          id_tag: education-case-studies
        case_studies:
          - title: Dublin City University Empowers Students
            author: by GitLab
            link_text: Read more
            image_url: /nuxt-images/case-study-logos/Dublin_City_University_Logo.svg
            image_alt: Dublin City University Logo
            link_url: /customers/dublin-city-university
            data_ga_name: Dublin City
            data_ga_location: body
          - title: How Heriot-Watt University scales for success with GitLab
            author: by GitLab
            link_text: Read more
            image_url: /nuxt-images/case-study-logos/hwlogo.png
            image_alt: Heriot Watt University Logo
            link_url: /customers/heriot_watt_university/
            data_ga_name: Heriot Watt
            data_ga_location: body
          - title: The University of Surrey achieves top marks with GitLab
            author: by GitLab
            link_text: Read more
            image_url: /nuxt-images/case-study-logos/logo_uni_surrey.svg
            image_alt: The University of Surrey Logo
            link_url: /customers/university-of-surrey/
            data_ga_name: University of Surrey
            data_ga_location: body
    - name: copy-media
      data:
        block:
          - header: DevOps in Education
            miscellaneous: |
              DevOps + Education: Are you interested in learning more about how the two relate? We are too! You've come to the right place!

              There are so many different ways DevOps can be used in education. We’ve been taking note of a few of the really great ways we see DevOps entering both the higher education classroom as well as K-12 and less formal settings such as clubs and boot camps!
            metadata:
              id_tag: devops-in-education
            hide_horizontal_rule: true
    - name: image-card
      data:
        block:
          - header: Open Science and research
            text: |
              Git technology is being used to manage data and publications. [Continuous integration (CI)](/features/continuous-integration/){data-ga-name="continuous integration" data-ga-location="body"} runs analytical workflows on data. Containerization allows for reproducability and versioned analysis. Collaborative tools speed up the cycle to results. Repositories store open data and web publications.
            icon:
              name: benefit
              alt: Benefit Icon
              variant: marketing
          - header: Learn computer science, coding, DevOps, and open source
            text: |
              Central repositories and source control are great tools for learning coding. Students can learn the stages of the DevOps lifecycle easily with a single platform. Continous integration tools can quality check and grade code assignments. Collaboration for group projects is easier with a central repositories, issues, epics and milestones.
            icon:
              name: code-alt-2
              alt: Code Icon
              variant: marketing
          - header: Student portfolios
            text: |
              Students can showcase contributions and code on a GitLab repository. GitLab profiles record a students' contributions and collaboration. GitLab can be used to host and publish blog posts, resumes, and other websites. Demonstrating use of GitLab adds credibility to DevOps skills and working knowledge.
            icon:
              name: pipeline-list
              alt: Pipeline List Icon
              variant: marketing
    - name: copy-media
      data:
        block:
          - header: GitLab in Academic Research
            miscellaneous: |
              Researchers around the world are conducting scientific research about GitLab, the company and the product, as well as using GitLab the product in thier research. The word "GitLab" appears in the title of **over 100 peer-reviewed articles and in the body of over 33k peer-review articles!** These articles cover a wide-range of topics including GitLab's all-remote operating model, continuous integration/continuous development (CI/CD), GitLab for library science, GitLab for measuring student collaboration, GitLab for deep-machine learning and so many more. Check out a small sample of highlights below.
            metadata:
              id_tag: gitlab-in-research
            hide_horizontal_rule: true
    - name: slp-table
      data:
        full_width: true
        headers:
          - Citation
          - Keywords
        rows:
          - columns:
              - "Araujo, G. G. and Kyrilov, A. (2020) '[Plagiarism Prevention through Project Based Learning with GitLab](http://www.ccsc.org/publications/journals/SW2020.pdf#page=53)', The Journal of Computing Sciences in Colleges, pp. 53."
              - |
                teaching and learning;
                project based learning
          - columns:
              - "Arefeen, M. S. and Schiller, M. (2019) '[Continuous Integration Using Gitlab](https://pdfs.semanticscholar.org/6398/66b94346070f157afd88d5cc25f4c3f54d2f.pdf)', Undergraduate Research in Natural and Clinical Science and Technology Journal, pp. 1-6."
              - CI
          - columns:
              - "Burr, C. and Couturier, B. '[A gateway between GitLab CI and DIRAC](https://cds.cern.ch/record/2752839/files/Fulltext.pdf)', EPJ Web of Conferences: EDP Sciences, 05026."
              - CI
          - columns:
              - "Choudhury, P., Crowston, K., Dahlander, L., Minervini, M. S. and Raghuram, S. (2020) '[GitLab: work where you want, when you want](https://www.hbs.edu/faculty/Pages/item.aspx?num=59235)', Journal of Organization Design, 9(1), pp. 1-17."
              - all-remote
          - columns:
              - "Choudhury, P. and Salomon, E. (2020a) '[GitLab and the Future of All-Remote Work (A)](https://www.hbs.edu/faculty/Pages/item.aspx?num=57917)', Harvard Business School Case, pp. 620-066."
              - |
                all-remote;
                case study
          - columns:
              - "Choudhury, P. and Salomon, E. (2020b) '[GitLab and the Future of All-Remote Work (B)](https://www.hbs.edu/faculty/Pages/item.aspx?num=57927)', Harvard Business School Supplement pp. 620-117."
              - |
                all-remote;
                case study
          - columns:
              - "Engwall, K. and Roe, M. (2020) '[Git and GitLab in library website change management workflows](https://journal.code4lib.org/articles/15250)', Code4Lib Journal, (48)."
              - |
                library science;
                change management
          - columns:
              - "Eraslan, S., Kopec-Harding, K., Jay, C., Embury, S. M., Haines, R., Ríos, J. C. C. and Crowther, P. (2020) '[Integrating GitLab Metrics into Coursework Consultation Sessions in a Software Engineering Course](https://www.sciencedirect.com/science/article/pii/S0164121220300911?casa_token=22g_wDh7S5YAAAAA:jwVdmxMF46c2WPItOrokgE4yE8UQx2VnwWv5Voow7VEUGuUkreaxfAUo8Nbo9jtCmiS96kgT)', Journal of Systems and Software, pp. 110613."
              - teaching and learning
          - columns:
              - "Puranam, P., Minervini, M. and Wee, J. 2020. '[GitLab: Can \"All Remote\" Scale?](https://publishing.insead.edu/case/gitlab)', INSEAD Case Study: INSEAD."
              - |
                all-remote;
                case study
          - columns:
              - "Than, P. P. and Phyu, M. P. '[Continuous integration for Laravel applications with GitLab](https://dl.acm.org/doi/abs/10.1145/3373477.3373479)', iProceedings of the International Conference on Advanced Information Science and System, 1-6."
              - CI
          - columns:
              - "Vassallo, C., Proksch, S., Jancso, A., Gall, H. C. and Di Penta, M. '[Configuration smells in continuous delivery pipelines: a linter and a six-month study on GitLab](https://dl.acm.org/doi/abs/10.1145/3368089.3409709)', iProceedings of the 28th ACM Joint Meeting on European Software Engineering Conference and Symposium on the Foundations of Software Engineering, 327-337."
              - CI
          - columns:
              - "Wang, J., Meng, X., Wang, H. and Sun, H. '[An Online Developer Profiling Tool Based on Analysis of GitLab Repositories](https://link.springer.com/chapter/10.1007/978-981-15-1377-0_32)', CCF Conference on Computer Supported Cooperative Work and Social Computing: Springeri, 408-417."
              - |
                professional development;
                portfolio development

