---
  title: Integrating security into your DevOps Lifecycle
  description: GitLab application security testing for SAST, DAST, Dependency scanning, Container Scanning and more within the DevSecOps CI pipeline with vulnerability management and compliance.
  template: 'industry'
  components:
    - name: 'solutions-hero'
      data:
        note: 
          -  Comprehensive DevSecOps
        title: GitLab Security and Governance
        subtitle: GitLab empowers your teams to balance speed and security by automating software delivery and securing your end-to-end software supply chain.
        aos_animation: fade-down
        aos_duration: 500
        aos_offset: 200
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          text: Try Ultimate for Free
          url: /free-trial/
          data_ga_name: free trial
          data_ga_location: hero
        secondary_btn:
          text: Questions? Contact us
          url: /sales/
          data_ga_name: sales
          data_ga_location: hero
        image:
          image_url: /nuxt-images/solutions/gitlab-security-and-governance/gitlab-security-and-governance.jpg
          image_url_mobile: /nuxt-images/gitlab-security-and-governance/gitlab-security-and-governance.jpg
          alt: "Image: gitlab for public sector"
          bordered: true
    - name: 'by-industry-intro'
      data:
        intro_text: Trusted by
        logos:
          - name: UBS Logo
            image: "/nuxt-images/home/logo_ubs_mono.svg"
            url: https://about.gitlab.com/blog/2021/08/04/ubs-gitlab-devops-platform/
            aria_label: Link to UBS customer case study
          - name: Hackerone Logo
            image: /nuxt-images/logos/hackerone-logo.png
            url: https://about.gitlab.com/customers/hackerone/
            aria_label: Link to US Army Cyber School customer case study
          - name: The Zebra Logo
            image: /nuxt-images/logos/zebra.svg
            url: https://about.gitlab.com/customers/thezebra/
            aria_label: Link to The Zebra customer case study
          - name: Hilti Logo
            image: /nuxt-images/logos/hilti_logo.svg
            url: https://about.gitlab.com/customers/hilti/
            aria_label: Link to Hilti customer case study
          - name: Conversica Logo
            image: /nuxt-images/logos/conversica.svg
            url: https://about.gitlab.com/customers/conversica/
            aria_label: Link to Conversica customer case study
          - name: Bendigo and Adelaide Bank Logo
            image: /nuxt-images/logos/bendigo_and_adelaide_bank.svg
            url: https://about.gitlab.com/customers/bab/
            aria_label: Link to Bendigo and Adelaide Bank customer case study
          - name: Glympse Logo
            image: /nuxt-images/logos/glympse-logo-mono.svg
            url: https://about.gitlab.com/customers/glympse/
            aria_label: Link to Glympse customer case study
    - name: 'side-navigation'
      links:
        - title: Overview
          href: '#overview'
        - title: Benefits
          href: '#benefits'
        - title: Capabilities
          href: '#capabilities'
        - title: Customers
          href: '#customers'
        - title: Pricing
          href: '#pricing'
        - title: Resources
          href: '#resources'
      slot_enabled: true
      slot_offset: 1
      slot_content:
        - name: 'div'
          id: 'overview'
          slot_enabled: true
          slot_content: 
            - name: 'by-solution-benefits'
              offset: 2
              data:
                title: Ship with speed and security
                is_accordion: false
                items:
                  - icon:
                      name: continuous-integration
                      alt: Continuous Integration Icon
                      variant: marketing
                    header: Integrated security
                    text: One platform, one price, everything out of the box.
                    link_text: Learn more about DevSecOps
                    link_url: /solutions/dev-sec-ops
                    ga_name: reduce security learn more
                    ga_location: benefits
                  - icon:
                      name: devsecops
                      alt: Devsecops Icon
                      variant: marketing
                    header: Continuous security
                    text: Automated scans before and after code push.
                    link_text: Why GitLab
                    link_url: /why-gitlab/
                    ga_name: free up learn more
                    ga_location: benefits
                  - icon:
                      name: shield-check-alt
                      alt: Shield Check Icon
                      variant: marketing
                    header: Complete control
                    text: Implement guardrails and automate policies.
                    link_text: Learn more about our platform approach
                    link_url: /solutions/devops-platform/
            - name: 'solutions-video-feature'
              offset: 2
              data:
                video: 
                  url: 'https://www.youtube.com/embed/XnYstHObqlA'
        - name: 'div'
          id: 'benefits'
          slot_enabled: true
          slot_content:
            - name: 'by-solution-value-prop'
              offset: 2
              data:
                title: Security. Compliance. Shifted left.
                cards:
                  - title: Secure your software supply chain
                    description: GitLab helps you secure your end-to-end software supply chain (including your source, build, dependencies, and released artifacts), create an inventory of software used (software bill of materials), and apply necessary controls.
                    icon:
                      name: less-risk
                      alt: Less Risk Icon
                      variant: marketing
                    href: /solutions/supply-chain/
                  - title: Manage threat vectors
                    description: GitLab helps you shift security left by automatically scanning vulnerabilities in source code, containers, dependencies, and running applications. Guardrail controls can be put in place to secure your production environment.
                    icon:
                      name: devsecops
                      alt: DevSecOps Icon
                      variant: marketing
                    href: https://docs.gitlab.com/ee/user/application_security/get-started-security.html
                  - title: Adhere to compliance requirements
                    description: GitLab can help you track your changes, implement necessary controls to protect what goes into production, and ensure adherence to license compliance and regulatory frameworks.
                    icon:
                      name: eye-magnifying-glass
                      alt: Eye Magnifying Glass Icon
                      variant: marketing
                    href: /solutions/compliance/
        - name: 'div'
          id: 'capabilities'
          slot_enabled: true
          slot_content:
            - name: 'by-industry-solutions-block'
              offset: 2
              data:
                subtitle: Implementing DevSecOps
                sub_description: ''
                white_bg: true
                sub_image: /nuxt-images/solutions/dev-sec-ops/dev-sec-ops.png
                solutions:
                  - title: Integrate security testing within the CI/CD pipeline
                    description: Use our built-in scanners and integrate custom scanners. Shift security left to empower developers to find and fix security flaws as they are created. Comprehensive scanners include SAST, DAST, secret scanning, dependency scanning, container scanning, IaC scanning, API security, and fuzz testing.
                    link_text: Learn More
                    link_url: https://docs.gitlab.com/ee/user/application_security/?_gl=1*hwzvlj*_ga*NTg0MjExODQyLjE2MTk1MzkzOTQ.*_ga_ENFH3X7M5Y*MTY2MzIyOTY1My44OS4xLjE2NjMyMzAyNTYuMC4wLjA.
                    data_ga_name: ci/cd
                    data_ga_location: solutions block
                  - title: Manage dependencies
                    description: Given the multitude of open source components that are now used in software development, manually managing these dependencies is a daunting task. Scan application and container dependencies for security flaws and create a software bill of materials (SBOM) of the dependencies used.
                    link_text: Learn More
                    link_url: https://docs.gitlab.com/ee/user/application_security/dependency_list/
                    data_ga_name: dependencies management
                    data_ga_location: solutions block
                  - title: Manage vulnerabilities
                    description: Scale security teams by surfacing vulnerabilities in developers’ natural workflow and resolving before pushing code to production. Security pros can vet, triage, and manage vulnerabilities from pipelines, on-demand scans, third parties, and bug bounties all in one place. 
                    link_text: Learn More
                    link_url: https://docs.gitlab.com/ee/user/application_security/vulnerability_report/
                    data_ga_name: vulnerability management
                    data_ga_location: solutions block
                  - title: Secure running applications
                    description: Protect your workloads by setting up a secure CI/CD tunnel with your clusters, running dynamic application security scanning, operational container scanning, and setting up IP whitelisting.
                    link_text: Learn More
                    link_url: https://docs.gitlab.com/ee/user/application_security/
                    data_ga_name: secure applications
                    data_ga_location: solutions block
                  - title: Implement guardrails and ensure compliance
                    description: Automate security and compliance policies across your software development lifecycle. Compliant pipelines ensure pipeline policies are not circumvented, while common controls provide end-to-end guardrails.
                    link_text: Learn More
                    link_url: https://docs.gitlab.com/ee/administration/compliance.html
                    data_ga_name: guardrails guardrails
                    data_ga_location: solutions block
        - name: 'div'
          id: 'customers'
          slot_enabled: true
          slot_content:
            - name: 'by-industry-quotes-carousel'
              offset: 1
              data:
                align: left
                header: |
                  Trusted by enterprises.
                  <br />
                  Loved by developers.
                quotes:
                  - title_img:
                      url: /nuxt-images/logos/hackerone-logo.png
                      alt: Hackerone Logo
                    quote: GitLab is helping us catch security flaws early and it's integrated it into the developer's flow. An engineer can push code to GitLab CI, get that immediate feedback from one of many cascading audit steps and see if there's a security vulnerability built in there, and even build their own new step that might test a very specific security issue.
                    author: Mitch Trale
                    position: Head of Infrastrusture, Hackerone
                    ga_carousel: hackerone testimonial
                    url: /customers/hackerone/
                  - title_img:
                      url: /nuxt-images/logos/bendigo_and_adelaide_bank.svg
                      alt: Bendigo and Adelaide Bank Logo
                    quote: We now have an always-innovating solution that aligns with our goal of digital transformation.
                    author: Caio Trevisan
                    position: Head of Devops Enablement, Bendigo and Adelaide Bank
                    ga_carousel: bendigo and adelaide bank testimonial
                    url: /customers/bab/
                  - title_img:
                      url: /nuxt-images/logos/zebra.svg
                      alt: The Zebra Logo
                    quote: The biggest value (of GitLab) is that it allows the development teams to have a greater role in the deployment process. Previously only a few people really knew how things worked, and now pretty much the whole development organization knows how the CI pipeline works, can work with it, add new services, and get things into production without infrastructure being the bottleneck.
                    author: Dan Bereczki
                    position: Sr. Software Manager, The Zebra
                    ga_carousel: the zebra testimonial
                    url: /customers/thezebra/
                  - title_img:
                      url: /nuxt-images/logos/hilti_logo.svg
                      alt: Hilti Logo
                    quote: GitLab is bundled together like a suite and then ships with a very sophisticated installer. And then it somehow works. This is very nice if you're a company which just wants to get it up and running.
                    author: Daniel Widerin
                    position: Head of Software Delivery, HILTI
                    ga_carousel: hilti testimonial
                    url: /customers/hilti/
        - name: div
          id: 'pricing'
          slot_enabled: true
          slot_content:
            - name: 'tier-block'
              class: 'slp-mt-96'
              id: 'pricing'
              offset: 1
              data:
                header: Which tier is right for you?
                cta:
                  url: /pricing/
                  text: Learn more about pricing
                  data_ga_name: pricing
                  data_ga_location: free tier
                  aria_label: pricing
                tiers:
                  - id: free
                    title: Free
                    items:
                      - Static application security testing (SAST) and secrets detection
                      - Findings in json file
                    link:
                      href: /pricing/
                      text: Learn more
                      data_ga_name: pricing
                      data_ga_location: free tier
                      aria_label: free tier
                  - id: premium
                    title: Premium
                    items:
                      - Static application security testing (SAST) and secrets detection
                      - Findings in json file
                      - MR approvals and more common controls
                    link:
                      href: /pricing/
                      text: Learn more
                      data_ga_name: pricing
                      data_ga_location: premium tier
                      aria_label: premium tier
                  - id: ultimate
                    title: Ultimate
                    items:
                      - Everything in Premium plus
                      - Comprehensive security scanners include SAST, DAST, Secrets, dependencies, containers, IaC, APIs, cluster images, and fuzz testing
                      - Actionable results within the MR pipeline
                      - Compliance pipelines
                      - Security and Compliance dashboards
                      - Much more
                    link:
                      href: /pricing/
                      text: Learn more
                      data_ga_name: pricing
                      data_ga_location: ultimate tier
                      aria_label: ultimate tier
                    cta:
                      href: /free-trial/
                      text: Try Ultimate for Free
                      data_ga_name: pricing
                      data_ga_location: ultimate tier
                      aria_label: ultimate tier
        - name: 'div'
          id: 'resources'
          slot_enabled: true
          slot_content:
          - name: solutions-resource-cards
            offset: 2
            data:
              title: Related Resources
              align: left
              column_size: 4
              grouped: true
              cards:
                - icon:
                    name: video
                    variant: marketing
                    alt: Video Icon
                  event_type: "Video"
                  header: Shifting Security Left - DevSecOps Overview
                  link_text: "Watch now"
                  image: "/nuxt-images/solutions/dev-sec-ops/shifting-security-left.jpeg"
                  href: https://www.youtube.com/embed/XnYstHObqlA
                  data_ga_name: Shifting Security Left - DevSecOps Overview
                  data_ga_location: resource cards
                - icon:
                    name: video
                    variant: marketing
                    alt: Video Icon
                  event_type: "Video"
                  header: Managing Vulnerabilities and Enabling Separation of Duties with GitLab
                  link_text: "Watch now"
                  image: /nuxt-images/solutions/dev-sec-ops/managing-vulnerabilities.jpeg
                  href: https://www.youtube.com/embed/J5Frv7FZtnI
                  data_ga_name: Managing Vulnerabilities and Enabling Separation of Duties with GitLab
                  data_ga_location: resource cards
                - icon:
                    name: video
                    variant: marketing
                    alt: Video Icon
                  event_type: "Video"
                  header: GitLab 15 Release - New Security Features
                  link_text: "Watch now"
                  image: "/nuxt-images/solutions/dev-sec-ops/gitlab-15-release.jpeg"
                  href: https://www.youtube.com/embed/BasGVNvOFGo
                  data_ga_name: GitLab 15 Release - New Security Features
                  data_ga_location: resource cards
                - icon:
                    name: video
                    variant: marketing
                    alt: Video Icon
                  event_type: "Video"
                  header: SBOM and Attestation
                  link_text: "Watch now"
                  image: "/nuxt-images/solutions/compliance/sbom-and-attestation.jpeg"
                  href: https://www.youtube.com/embed/wX6aTZfpJv0
                  data_ga_name: SBOM and Attestation
                  data_ga_location: resource cards
                - icon:
                    name: ebook
                    variant: marketing
                    alt: Ebook Icon
                  event_type: "Book"
                  header: "Guide to software supply chain security"
                  link_text: "Learn more"
                  image: "/nuxt-images/resources/resources_11.jpeg"
                  href: https://cdn.pathfactory.com/assets/10519/contents/360915/35d042c6-3449-4d50-b2e9-b08d9a68f7a1.pdf
                  data_ga_name: "Guide to software supply chain security"
                  data_ga_location: resource cards
                - icon:
                    name: ebook
                    variant: marketing
                    alt: Ebook Icon
                  event_type: "Book"
                  header: "DevSecOps with GitLab CI/CD"
                  link_text: "Learn more"
                  image: "/nuxt-images/resources/resources_1.jpeg"
                  href: https://page.gitlab.com/achieve-devsecops-cicd-ebook.html
                  data_ga_name: "DevSecOps with GitLab CI/CD"
                  data_ga_location: resource cards
                - icon:
                    name: ebook
                    variant: marketing
                    alt: Ebook Icon
                  event_type: "Book"
                  header: "GitLab DevSecOps survey"
                  link_text: "Learn more"
                  image: "/nuxt-images/resources/fallback/img-fallback-cards-infinity.png"
                  href: https://cdn.pathfactory.com/assets/10519/contents/432983/c6140cad-446b-4a6c-96b6-8524fac60f7d.pdf
                  data_ga_name: "GitLab DevSecOps survey"
                  data_ga_location: resource cards
                - icon:
                    name: blog
                    variant: marketing
                    alt: Blog Icon
                  event_type: "Blog"
                  header: "9 tips to shift left"
                  link_text: "Learn more"
                  href: https://about.gitlab.com/blog/2020/06/23/efficient-devsecops-nine-tips-shift-left/
                  image: /nuxt-images/blogimages/hotjar.jpg
                  data_ga_name: "9 tips to shift left"
                  data_ga_location: resource cards
                - icon:
                    name: blog
                    variant: marketing
                    alt: Blog Icon
                  event_type: "Blog"
                  header: "Securing the software supply chain through automated attestation"
                  link_text: "Learn more"
                  href: https://about.gitlab.com/blog/2022/08/10/securing-the-software-supply-chain-through-automated-attestation/
                  image: /nuxt-images/blogimages/cover_image_regenhu.jpg
                  data_ga_name: "Securing the software supply chain through automated attestation"
                  data_ga_location: resource cards
                - icon:
                    name: report
                    alt: Report Icon
                    variant: marketing
                  event_type: "Analyst Report"
                  header: "451 Research opinion: GitLab broadens view of DevOps"
                  link_text: "Learn more"
                  href: "https://page.gitlab.com/resources-report-451-gitlab-broadens-devops.html"
                  image: /nuxt-images/blogimages/hemmersbach_case_study.jpg
                  data_ga_name: "451 Research opinion: GitLab broadens view of DevOps"
                  data_ga_location: resource cards
                - icon:
                    name: report
                    alt: Report Icon
                    variant: marketing
                  event_type: "Analyst Report"
                  header: "GitLab a challenger in 2022 Gartner Magic Quadrant"
                  link_text: "Learn more"
                  href: "https://about.gitlab.com/analysts/gartner-ast22/"
                  image: /nuxt-images/blogimages/fjct_cover.jpg
                  data_ga_name: "GitLab a challenger in 2022 Gartner Magic Quadrant"
                  data_ga_location: resource cards
    - name: solutions-cards
      data:
        title: Do more with GitLab
        column_size: 4
        link :
          url: /solutions/
          text: Explore more Solutions
          data_ga_name: soluions explore more
          data_ga_location: body
        cards:
          - title: Continuous Software Compliance
            description: Integrating security into your DevOps lifecycle is easy with GitLab.
            icon:
              name: continuous-integration
              alt: Continuous Integration Icon
              variant: marketing
            href: /solutions/compliance/
            data_ga_name: continuous software compliance learn more
            data_ga_location: body
          - title: Software Supply Chain Security
            description: Ensure your software supply chain is secure and compliant.
            icon:
              name: devsecops
              alt: Devsecops Icon
              variant: marketing
            href: /solutions/supply-chain/
            data_ga_name: software supply chain security learn more
            data_ga_location: body
          - title: Continuous Integration and Delivery
            description: Make software delivery repeatable and on-demand
            icon:
              name: continuous-delivery
              alt: Continuous Delivery
              variant: marketing
            href: /features/continuous-integration/
            data_ga_name: siemens learn more
            data_ga_location: body

                  