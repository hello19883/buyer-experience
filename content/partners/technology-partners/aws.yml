---
  title: GitLab on AWS
  description: Unblock your process with a complete DevOps platform to build, test, and deploy on AWS.
  components:
    - name: partners-call-to-action
      data:
        title: GitLab on AWS
        inverted: true
        body:
          - text: Unblock your process with a complete DevOps platform to build, test, and deploy on AWS.
        image:
          image_url: /nuxt-images/partners/aws/aws-logo.svg
          alt: AWS logo
    - name: copy-media
      data:
        block:
          - header: Deploy anywhere with everyone
            text: |
              GitLab is a DevOps platform with bring-your-own-infrastructure flexibility. From the on-premise to cloud, run GitLab on AWS and deploy to your workloads and AWS infrastructure using a single solution for everyone on your pipeline.
            image:
              image_url: /nuxt-images/partners/aws/featured-partner-aws-1.png
            link_href: /resources/whitepaper-deploy-aws-gitlab/
            link_text: Read the whitepaper
            secondary_link_href: https://aws.amazon.com/marketplace/pp/GitLab-GitLab-Ultimate/B07SJ817DX
            secondary_link_text: Start your free trial
    - name: 'partners-feature-showcase'
      data:
        header: Develop better cloud native applications faster with GitLab and AWS
        image_url: /nuxt-images/partners/aws/featured-partner-aws-2.png
        text: GitLab collapses cycle times by driving efficiency at every stage of your software development process – from idea to deploying on AWS. GitLab’s complete DevOps platform delivers built-in planning, monitoring, and reporting solutions plus tight AWS integration for any workload.
        cards:
          - header: Rapid collaboration
            text: Contribute with purpose. Version control and collaboration reduce rework so happier developers can expand product roadmaps instead of repairing old roads.
          - header: Reliable workflows
            text: Deploy unstoppable software with DevSecOps. Automated workflows increase uptime by reducing security and compliance risks on AWS.
          - header: Repeatable results
            text: Stay in the game to win it. Increase market share and revenue when your product is on budget, on time, and always up.
    - name: 'benefits'
      data:
        header: Get started with GitLab and AWS joint solutions
        description: |
          As a certified AWS Advanced Technology Partner with DevOps Competency, GitLab CI/CD is a proven model for customer success with the leading cloud platform. AWS customers can choose from two deployment options: **GitLab self-managed** and **GitLab SaaS**.
          \
          Install, administer, and maintain your own GitLab instance that runs on everything from bare metal, VMs, and containers on AWS with GitLab self-managed. GitLab SaaS requires no installation, so you can sign up and get started quickly.
        full_background: true
        cards_per_row: 2
        text_align: left
        benefits:
          - icon:
              name: cloud-circle
              alt: Cloud Circle Icon
              variant: marketing
              hex_color: '#2F80ED'
            title: Amazon Elastic Compute Cloud (EC2)
            description: Amazon EC2 provides scalable AWS cloud computing capacity. GitLab scales jobs across multiple machines. When used together, GitLab on EC2 can significantly reduce infrastructure costs.
            link:
              text: Learn more
              url: https://docs.gitlab.com/ee/install/aws/
          - icon:
              name: currency-circle
              alt: Currency Circle Icon
              variant: marketing
              hex_color: '#1DBA9E'
            title: AWS Fargate
            description: With one click on GitLab, AWS Fargate enables scalable serverless container deployments. Organizations migrate to Fargate to optimize compute resources and save on infrastructure costs. Fargate works with an AWS stack that includes ECS or EKS.
            link:
              text: Learn more
              url: /customers/trek10/
          - icon:
              name: data-circle
              alt: Data Circle Icon
              variant: marketing
              hex_color: '#FCA121'
            title: Amazon Elastic Kubernetes Services (EKS)
            description: AWS Elastic Kubernetes Service (EKS) is a managed Kubernetes service. GitLab CI/CD offers integrated cluster creation for EKS. EKS is the only Kubernetes service that lets existing AWS users take advantage of the tight integration with other AWS services and features. GitLab also supports Amazon EKS-D.
            link:
              text: Learn more
              url: /blog/2020/03/09/gitlab-eks-integration-how-to/
          - icon:
              name: code-circle
              alt: Code Circle Icon
              variant: marketing
              hex_color: '#FA7035'
            title: AWS Lambda
            description: AWS Lambda is a computing service that runs code in response to events and automatically manages the computing resources required by that code. GitLab supports the development of Lambda functions and serverless applications with AWS Serverless Application Model (AWS SAM) and GitLab CI/CD.
            link:
              text: Learn more
              url: https://docs.gitlab.com/ee/user/project/clusters/serverless/aws.html
          - icon:
              name: timer-circle
              alt: Timer Circle Icon
              variant: marketing
              hex_color: '#EB5757'
            title: AWS Elastic Container Service (Amazon ECS)
            description: AWS Elastic Container Service (Amazon ECS) is a container management service. Save time when you run AWS commands from GitLab CI/CD and automate docker deployments with GitLab’s CI templates.
            link:
              text: Learn more
              url: https://docs.gitlab.com/ee/ci/cloud_deployment/#deploy-your-application-to-the-aws-elastic-container-service-ecs
          - icon:
              name: branch-circle
              alt: Branch Circle Icon
              variant: marketing
              hex_color: '#9B51E0'
            title: Windows .Net on AWS
            description: GitLab enables CI/CD for Windows .Net applications on AWS. Automatically deploy containerized applications, including serverless resources, with GitLab on Lambda or Fargate.
            link:
              text: Learn more
              url: https://youtu.be/_4r79ZLmDuo
    - name: 'pull-quote'
      data:
        quote: The feature set with GitLab on-premise was more advanced than GitHub, and we saw the pace and development [of GitLab] moving faster with a community that was active in delivering and contributing.
        source: ERIC LABOURDETTE, HEAD OF GLOBAL R&D ENGINEERING SERVICES AT AXWAY
        link_text: ''
        shadow: true
    - name: copy-resources
      data:
        title: Discover the benefits of GitLab on AWS
        block:
          - video:
              title: 'Opening Keynote: The Power of GitLab - Sid Sijbrandij'
              video_url: https://www.youtube.com/embed/xn_WP4K9dl8?enablesjsapi=1
              label: Gitlab commit virtual 2020
            resources:
              video:
                header: Video
                links:
                  - text: Northwestern Mutual deployed GitLab Runners in AWS to manage demand for capacity and cost
                    link: https://www.youtube.com/watch?v=K6OS8WodRBQ
                  - text: WagLabs is achieving the impossible by using self-hosted GitLab on AWS to power its CI/CD
                    link: https://www.youtube.com/watch?v=HfEl9GXZC0s
              webcast:
                header: Webcasts
                links:
                  - text: Deploy AWS Lambda applications with ease
                    link: /webcast/aws-gitlab-serverless/
                  - text: Up-level your AWS infrastructure automation with GitOps
                    link: https://www.youtube.com/watch?v=QgD8Jz22TXg
                  - text: Ask Media Group made the shift from on-prem data centers to the AWS cloud by leveraging GitLab Runners
                    link: /webcast/cloud-native-transformation/
                  - text: GitLab + HeleCloud - Leveraging GitOps to improve your operational efficiencies with AWS
                    link: https://www.youtube.com/watch?v=eiZUVPeyktI&t=391s
              whitepaper:
                header: Whitepapers
                links:
                  - text: How to deploy on AWS from GitLab
                    link: /resources/whitepaper-deploy-aws-gitlab/
                  - text: How GitLab accelerates workload deployments on AWS
                    link: https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/uploads/59d1390557bb304e7401443c4e710c0f/gitlab-aws-ci_t-campaign-infographic-01.pdf
                  - text: Deploy your software to AWS you get the best DevOps tooling running on the best cloud infrastructure
                    link: /resources/downloads/GitLab_AWS_Solution_Brief.pdf
              blog:
                header: Blogs
                links:
                  - text: CI/CD with Amazon EKS using AWS App Mesh and Gitlab CI
                    link: https://aws.amazon.com/blogs/containers/ci-cd-with-amazon-eks-using-aws-app-mesh-and-gitlab-ci/
                  - text: How to stand up a Gitab instance in AWS Marketplace
                    link: /blog/2021/06/30/how-to-stand-up-gitlab-in-awsmp/
