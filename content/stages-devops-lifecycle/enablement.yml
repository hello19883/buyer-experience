---
  title: Enablement
  description: Learn more about GitLab enablement stage in the DevOps lifecycle.
  components:
    - name: call-to-action
      data:
        aos_animation: fade-down
        aos_duration: 500
        title: Enablement
        centered_by_default: true
    - name: featured-media
      data:
        header: Product categories
        header_animation: fade-up
        header_animation_duration: 500
        column_size: 6
        media:
          - title: Global Search
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              Global Search is made up of two primary experiences, Basic Search and Advanced Search. Basic Search is the default search experience for self-managed users as well as Free users of GitLab.com. It provides a way to search across the DevOps platform. Basic search includes Code Search for one project. Advanced Search, in GitLab Premium and above, is an optional feature that uses Elasticsearch to enable additional search features for sorting, filtering, and improved relevancy.
            link:
              href: https://docs.gitlab.com/ee/user/search/advanced_search.html
              text: Learn More
              data_ga_name: global search learn more
              data_ga_location: body
          - title: Code Search
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              Code Search gives users the ability to explore all their code. Code Search is the fundamental need for organizations with complex or large amounts of code.
            link:
              href: https://docs.gitlab.com/ee/user/search/advanced_search.html
              text: Learn More
              data_ga_name: code search learn more
              data_ga_location: body
          - title: Omnibus Package
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              Install a self-managed instance of GitLab using the Omnibus package.
            link:
              href: https://docs.gitlab.com/omnibus/
              text: Learn More
              data_ga_name: omnibus package learn more
              data_ga_location: body
          - title: Cloud Native Installation
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              Install GitLab in a cloud native environment using Helm.
            link:
              href: https://docs.gitlab.com/charts/
              text: Learn More
              data_ga_name: cloud native installation learn more
              data_ga_location: body
          - title: Geo-replication
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              Geo-replication provides an easily configurable, read-only mirror (we call it a Geo node) of a GitLab installation that is complete, accurate, verifiable and efficient.
            link:
              href: /solutions/geo/
              text: Learn More
              data_ga_name: geo-replication learn more
              data_ga_location: body
          - title: Disaster Recovery
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              Disaster Recovery (DR) helps our customers fulfill their business continuity plans by creating processes that allow the recovery of GitLab following a natural or human-created disaster.
            link:
              href: https://docs.gitlab.com/ee/administration/geo/disaster_recovery/
              text: Learn More
              data_ga_name: disaster recovery learn more
              data_ga_location: body
    - name: copy
      data:
        aos_animation: fade-up
        aos_duration: 500
        block:
          - align_center: true
            text: |
              Learn more about our roadmap for upcoming features on our [Direction page](/direction/enablement/){data-ga-name="enablement direction" data-ga-location="body"}.
    - name: sdl-related-card
      data:
        column_size: 4
        header: Related
        cards:
          - title: Create
            icon:
              name: create-alt-2
              alt: Create Icon
              variant: marketing
            aos_animation: zoom-in
            aos_duration: 500
            text: Create, view, manage code and project data through powerful branching tools.
            link:
              text: Learn More
              href: /stages-devops-lifecycle/create/
              data_ga_name: create
              data_ga_location: body
          - title: Plan
            icon:
              name: plan-alt-2
              alt: Plan Icon
              variant: marketing
            aos_animation: zoom-in
            aos_duration: 1000
            text: Regardless of your process, GitLab provides powerful planning tools to keep everyone synchronized.
            link:
              text: Learn More
              href: /stages-devops-lifecycle/plan/
              data_ga_name: plan
              data_ga_location: body
          - title: Manage
            icon:
              name: manage-alt-2
              alt: Manage Icon
              variant: marketing
            aos_animation: zoom-in
            aos_duration: 1500
            text: Gain visibility and insight into how your business is performing.
            link:
              text: Learn More
              href: /stages-devops-lifecycle/manage/
              data_ga_name: manage
              data_ga_location: body
